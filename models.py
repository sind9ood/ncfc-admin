from sqlalchemy.schema import Sequence

from flaskr.flaskr_app import db
from datetime import datetime

POST_ID_SEQ = Sequence('post_id_seq')  # define sequence explicitly


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def to_dict(self):
        return {'id': self.id,
                'username': self.username,
                'email': self.email,
                'password': self.password}


class Post(db.Model):
    id = db.Column(db.Integer, POST_ID_SEQ, primary_key=True, server_default=POST_ID_SEQ.next_value())
    title = db.Column(db.String(50))
    body = db.Column(db.String(1000))
    created = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

    def to_dict(self):
        return {'id': self.id,
                'title': self.title,
                'body': self.body,
                'created': self.created,
                'author_id': self.author_id}