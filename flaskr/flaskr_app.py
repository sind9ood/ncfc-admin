import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# create and configure the app
app = Flask(__name__, instance_relative_config=True)
app.config.from_object(os.environ['APP_SETTINGS'])
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


from . import auth
app.register_blueprint(auth.bp)

from . import blog
app.register_blueprint(blog.bp)

from . import upload
app.register_blueprint(upload.bp)

from . import report
app.register_blueprint(report.bp)
app.add_url_rule('/', endpoint='index')


if __name__ == '__main__':
    app.run()