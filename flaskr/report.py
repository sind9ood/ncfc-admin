from multiprocessing.pool import ThreadPool

import os

from jinja2 import Environment
from jinja2.loaders import FileSystemLoader

from flask import flash, request, redirect, url_for, Blueprint, render_template, Response

from flaskr.auth import login_required, admin_required
from flaskr.utils import build_result, get_api, info_repr, valid_form_breeze_id, error_repr, \
    get_bid_name_mappings_from_breeze, get_sorted_funds

bp = Blueprint('report', __name__, url_prefix='/report')


@bp.route('/', methods=['GET', 'POST'])
@login_required
@admin_required
def index():
    if request.method == 'POST':
        # check if the post request has the batch number
        if 'batchnum' not in request.form:
            flash('No batch number')
            return redirect(request.url)
        batchnum = request.form['batchnum']
        return redirect(url_for('report.complete', batchnum=batchnum))
    return render_template('report/index.html')


@bp.route('/complete')
def complete():
    batchnum = request.args['batchnum']

    def inner():
        try:
            yield info_repr("Preparing Data")
            breeze_api = get_api()
            p_dict = get_bid_name_mappings_from_breeze(breeze_api)
            sorted_funds = get_sorted_funds(breeze_api)

            yield info_repr("Building Report")
            fund_summary, note_summary = {}, {}

            sorted_fund_strs = []
            pool = ThreadPool(processes=8)
            total = 0
            batch_date = None
            fund_result = {}
            for i, fund in enumerate(sorted_funds):
                fund_str = '. '.join([fund['custom_id'], fund['name']])
                fund_result[fund_str] = pool.apply_async(get_fund_info, (breeze_api, fund,  batchnum, p_dict))

            for i, fund in enumerate(sorted_funds):
                fund_str = '. '.join([fund['custom_id'], fund['name']])
                if fund_result[fund_str] is not None:
                    res = fund_result[fund_str].get()
                    if res[0] > 0.0:
                        total += res[0]
                        sorted_fund_strs.append(fund_str)
                        fund_summary[fund_str] = res[2]
                        note_summary[fund_str] = res[3]
                    if batch_date is None:
                        batch_date = res[1]

            report_str = build_result(batchnum, batch_date, total, fund_summary, note_summary, sorted_fund_strs)
            yield "<br/>"
            yield "<pre/>" + report_str

        except RuntimeError as e:
            yield error_repr(e.message)

    templates_dir = os.path.join(bp.root_path, 'templates')
    env = Environment(loader=FileSystemLoader(templates_dir))
    tmpl = env.get_template('upload/complete.html')
    return Response(tmpl.generate(result=inner()))


def get_fund_info(breeze_api, fund, batch_num, p_dict):

    contributions = breeze_api.list_contributions(fund_ids=[fund['id']], batches=[batch_num])
    is_special_fund = int(fund['custom_id']) >= 1000

    fund_tot = 0
    batch_date = None
    note_summary = []
    for c in contributions:
        fund_tot += float(c['amount'])

        if batch_date is None:
            batch_date = c['paid_on'].split()[0]

        bid = c['person_id']
        # Skip anonymous
        if valid_form_breeze_id(bid):
            p_name = p_dict[bid]

            if c['note'] != "":
                note_str = "$%s\t%s" % (c['amount'], c['note'])
                note_str += " :: %s" % p_name
                note_summary.append(note_str)
            elif is_special_fund:
                note_str = "$%s\t%s" % (c['amount'], fund['name'])
                note_str += " :: %s" % p_name
                note_summary.append(note_str)

    if fund_tot > 0.0:
        fund_summary = fund_tot
    else:
        fund_summary = None

    return fund_tot, batch_date, fund_summary, note_summary