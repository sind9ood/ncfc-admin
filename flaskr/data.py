import os
import xlrd
import pandas as pd

from utils import isNan, get_batch_name


def read_id_mappings_from_file(target_file):
    try:
        wb = xlrd.open_workbook(target_file)
        sheet = wb.sheet_by_index(3)

        def get_cell_range(start_col, start_row, end_col, end_row):
            return [sheet.row_slice(row, start_colx=start_col, end_colx=end_col + 1) for row in
                    xrange(start_row, end_row + 1)]

        cell_data = get_cell_range(1, 1, 4, 999)
        env_bid_mappings, bid_name_mappings = dict(), dict()
        for row in cell_data:
            if len(str(row[0].value)) == 0:
                break

            env_num = str(int(row[0].value))
            last_name, first_name = row[1].value.split(',')
            last_name = last_name.strip()
            first_name = first_name.lstrip()
            person_name = ' '.join([first_name, last_name])
            breeze_id = str(int(row[2].value))
            env_bid_mappings[env_num] = breeze_id
            bid_name_mappings[breeze_id] = person_name

    except Exception as e:
        return None, e.message

    return [env_bid_mappings, bid_name_mappings], "Successful in loading id mappings!"


# def read_id_mappings_from_file(target_file):
#
#     try:
#         df = pd.read_excel(target_file, sheet_name=3)
#         env_bid_mappings, bid_name_mappings = dict(), dict()
#
#         for row in df.iterrows():
#             if isNan(row[1]['Envelope ID']):
#                 break
#             env_num = str(int(row[1]['Envelope ID']))
#             last_name, first_name = row[1]['Member Name'].split(",")
#             person_name = ' '.join([first_name.lstrip(), last_name.strip()])
#             breeze_id = str(int(row[1]['Breeze ID']))
#
#             env_bid_mappings[env_num] = breeze_id
#             bid_name_mappings[breeze_id] = person_name
#
#     except Exception as e:
#         return None, e.message
#
#     return [env_bid_mappings, bid_name_mappings], "Successful in loading id mappings!"


# def read_giving_template(target_file):
#     try:
#         wb = xlrd.open_workbook(target_file)
#         sheet = wb.sheet_by_index(1)
#
#         def get_cell_range(start_col, start_row, end_col, end_row):
#             return [sheet.row_slice(row, start_colx=start_col, end_colx=end_col + 1) for row in
#                     xrange(start_row, end_row + 1)]
#
#         cell_data = get_cell_range(1, 1, 9, 299)
#         df = []
#         for row in cell_data:
#             if len(str(row[0].value)) == 0:
#                 break
#
#             instance = dict()
#             instance["env_num"] = str(int(row[0].value))
#             instance["amount"] = str(row[1].value)
#             instance["fund_id"] = str(row[3].value)
#             instance["fund_name"] = str(row[4].value)
#             instance["note"] = unicode(row[5].value)
#             instance["batch_num"] = str(int(row[6].value))
#
#             date_tuple = xlrd.xldate_as_tuple(row[7].value, wb.datemode)
#             a1_datetime = datetime(*date_tuple)
#             instance["date"] = a1_datetime.strftime("%d-%m-%Y")
#             instance["method"] = str(row[8].value)
#             instance["batch_name"] = get_batch_name(a1_datetime, instance["batch_num"], str(row[8].value))
#             df.append(instance)
#
#     except Exception as e:
#         return None, e.message
#
#     try:
#         os.remove(target_file)
#         print "Removed %s successfully!" % target_file
#     except Exception as e:
#         return None, e.message
#
#     return df, "Successful in loading excel file!"


def read_giving_template(target_file):

    try:
        df = pd.read_excel(target_file, sheet_name=1)
        instances = []

        for row in df.iterrows():
            instance = dict()
            if isNan(row[1]['Envelop #']):
                break
            instance["env_num"] = str(int(row[1]['Envelop #']))
            instance["amount"] = float(row[1]['Amount'])
            # handle cases where name is not present
            if isinstance(row[1]['Name Lookup'], float):
                last_name, first_name = '-', '-'
            else:
                last_name, first_name = row[1]['Name Lookup'].split(",")
            instance["p_name"] = ' '.join([first_name.lstrip(), last_name.strip()])
            instance["fund_id"] = str(int(row[1]['Fund #']))
            instance["fund_name"] = row[1]['Fund Name Lookup']
            note = unicode(row[1]['Note']) if not isNan(row[1]['Note']) else ""
            instance["note"] = note
            instance["batch_num"] = str(int(row[1]['Batch #']))
            giving_date = row[1]['Date Entered'].date()
            instance["date"] = giving_date.strftime("%d-%m-%Y")
            congregation = row[1]['Congregation']
            instance["method"] = congregation
            instance["batch_name"] = get_batch_name(giving_date, instance["batch_num"], congregation)
            instances.append(instance)

    except Exception as e:
        return None, e.message

    os.remove(target_file)
    print "Removed %s successfully!" % target_file

    return instances, "Successful in loading excel form"




