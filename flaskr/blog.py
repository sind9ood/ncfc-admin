from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.flaskr_app import db
from models import Post, User

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    posts = db.session.query(Post).order_by(Post.created).all()
    output_posts = []
    for post in posts:
        user = db.session.query(User).filter(User.id == post.author_id).first()
        output_post = post.to_dict()
        output_post["username"] = user.username
        output_posts.append(output_post)
    return render_template('blog/index.html', posts=output_posts)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            new_post = Post(id=None, title=title, body=body, author_id=g.user['id'])
            db.session.add(new_post)
            db.session.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


def get_post(id, check_author=True):
    post = db.session.query(Post).filter(Post.id == id).first()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post.author_id != g.user['id']:
        abort(403)

    return post


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            post.title = title
            post.body = body
            db.session.commit()
            return redirect(url_for('blog.index'))

    user = db.session.query(User).filter(User.id == post.author_id).first()
    post_contents = post.to_dict()
    post_contents["username"] = user.username

    return render_template('blog/update.html', post=post_contents)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    Post.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('blog.index'))