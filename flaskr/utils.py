import os
import json
import math

from datetime import datetime
from breeze import breeze


def get_api(breeze_url='https://ncfctraining.breezechms.com', api_key='ff92a8dc86810b514d8d3293dd188867'):
    return breeze.BreezeApi(breeze_url=breeze_url, api_key=api_key)

# def get_api(breeze_url='https://ncfcmd.breezechms.com', api_key='359f1843a8a725bdf23253397e684a53'):
#     return breeze.BreezeApi(breeze_url=breeze_url, api_key=api_key)


def isNan(val):
    if isinstance(val, float) and math.isnan(val):
        return True
    return False


def valid_form_envelope_id(eid):
    return len(eid) <= 4


def valid_form_breeze_id(bid):
    def check_value(val):
        return val >= 10000000

    if isinstance(bid, str):
        return bid.isdigit() and check_value(int(bid))
    elif isinstance(bid, unicode):
        return str(bid).isdigit() and check_value(int(str(bid)))
    elif isinstance(bid, int):
        return check_value(bid)
    else:
        return False


def get_bid_and_name(env_str, mappings=None, breeze_api=None):
    if valid_form_envelope_id(env_str) and mappings is not None:
        bid = mappings['env_bid'][env_str]
        p_name = mappings['bid_name'][bid]
    elif valid_form_breeze_id(env_str) and breeze_api is not None:
        bid = env_str
        person_details = breeze_api.get_person_details(bid)
        p_name = ' '.join([person_details['force_first_name'], person_details['last_name']])
    else:
        None, None

    return bid, p_name


def get_bid_name_mappings_from_breeze(breeze_api):
    people = breeze_api.get_people()
    return {person['id']:' '.join([person['force_first_name'], person['last_name']]) for person in people}


def get_sorted_funds(breeze_api):
    funds = breeze_api.list_funds()

    sorted_fund_ids = sorted([int(fund['custom_id']) for fund in funds])
    sorted_funds = []
    for fid in sorted_fund_ids:
        for fund in funds:
            if fund['custom_id'] == str(fid):
                sorted_funds.append(fund)
                break
    return sorted_funds


def load_id_mappings(tdir):
    try:
        env_bid_mappings = load_mappings("env_bid_mappings.json", tdir)
        bid_name_mappings = load_mappings("bid_name_mappings.json", tdir)
    except Exception as e:
        return None, None, e.message
    return env_bid_mappings, bid_name_mappings, "Successful in loading ID mappings!"


def load_mappings(fname, tdir):
    return json.load(open(os.path.join(tdir, fname)))


def persist(dict_obj, name, target_dir='mappings'):
    json_str = json.dumps(dict_obj)

    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    full_file_name = os.path.join(target_dir, name)
    f = open(full_file_name, "w")
    f.write(json_str)
    f.close()


def get_batch_name(fund_date, batch_num, congregation):
    str_date = datetime.strftime(fund_date, '%Y.%m%d')
    zeros = (4 - len(str(batch_num))) * "0"
    return zeros + str(batch_num) + '.' + str_date + '.' + congregation


def get_fund_summary(fund_summary, sorted_fund_strs=None):
    result = "\n"
    sorted_key_list = sorted_fund_strs if sorted_fund_strs else sorted(fund_summary.keys())
    for key in sorted_key_list:
        result += "\t%s : $%.2f\n" % (key, fund_summary[key])
    return result


def get_note_summary(note_summary, fund_summary, sorted_fund_strs=None):
    result = "\n"
    sorted_key_list = sorted_fund_strs if sorted_fund_strs else sorted(fund_summary.keys())
    for key in sorted_key_list:
        result += "\t%s : $%.2f\n" % (key, fund_summary[key])
        if key in note_summary:
            for t in note_summary[key]:
                result += "\t----- %s\n" % t
        result += "\n"
    return result


def build_result(batch_num, batch_date, total, fund_summary, note_summary, sorted_fund_strs=None):
    result = "Report of Batch %s (Week of %s)\n\n" % (batch_num, batch_date)

    result += "I. Total Amount : $%.2f\n" % total
    result += '\n\n'

    result += "II. Sub-total by Fund\n"
    result += get_fund_summary(fund_summary, sorted_fund_strs)
    result += '\n\n'

    result += "III. Fund Details\n"
    result += get_note_summary(note_summary, fund_summary, sorted_fund_strs)
    return result


def error_repr(msg):
    return "<b><font size=\"2\" font-weight: bold color=\"red\">[Error] " + msg + "</font></b></br>"


def info_repr(msg):
    return "<b><font color=\"#005CB9\" face=\"courier\" size=\"2\">[Info] " + msg + "</font></b></br>"