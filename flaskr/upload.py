import os
import tempfile
import json

from multiprocessing.pool import ThreadPool

from collections import defaultdict
from datetime import datetime

from jinja2 import Environment
from jinja2.loaders import FileSystemLoader

from flask import flash, request, redirect, url_for, Blueprint, render_template, Response
from werkzeug.utils import secure_filename

from flaskr.auth import login_required, admin_required
from flaskr.data import read_giving_template, read_id_mappings_from_file
from flaskr.utils import build_result, persist, load_id_mappings, get_api, error_repr, info_repr, valid_form_breeze_id, \
    get_bid_and_name, valid_form_envelope_id, get_bid_name_mappings_from_breeze

bp = Blueprint('upload', __name__, url_prefix='/upload')


ALLOWED_EXTENSIONS = set(['xls', 'xlsx'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@bp.route('/', methods=['GET', 'POST'])
@login_required
@admin_required
def index():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            temp_dir = tempfile.gettempdir()
            file.save(os.path.join(temp_dir, filename))

            # persist envelope-bid mappings
            mappings, msg = read_id_mappings_from_file(os.path.join(temp_dir, filename))
            persist(mappings[0], "env_bid_mappings.json", os.path.join(bp.root_path, 'mappings'))
            persist(mappings[1], "bid_name_mappings.json", os.path.join(bp.root_path, 'mappings'))
            return redirect(url_for('upload.uploaded', filename=filename, tempdir=temp_dir))
    return render_template('upload/index.html')


@bp.route('/uploaded/<filename>', methods=['GET', 'POST'])
def uploaded(filename):
    if request.method == 'POST':
        full_path = os.path.join(request.args["tempdir"], filename)
        return redirect(url_for('upload.complete', filename=full_path))
    return render_template('upload/uploaded.html', filename=filename)


@bp.route('/complete')
def complete():
    filename = request.args['filename']
    breeze_api = get_api()

    mappings = {}
    mappings['env_bid'], mappings['bid_name'], msg = load_id_mappings(os.path.join(bp.root_path, 'mappings'))

    def inner():
        try:
            df, msg = read_giving_template(filename)

            if df is None:
                yield error_repr(msg)
            else:
                text1 = 'Loaded %s transactions' % len(df)
                yield info_repr(text1)
                yield info_repr(msg)

                orig_data, msg = validate_dates_nums(df)

                if orig_data is None:
                    yield error_repr(msg)
                else:
                    yield info_repr(msg)

                    env_list, msg = validate_envelop_nums(df, mappings['env_bid'])
                    if env_list is None:
                        yield error_repr(msg)
                    else:
                        yield info_repr(msg)

                        bid_name_mappings_br = get_bid_name_mappings_from_breeze(breeze_api)
                        people_list, msg = validate_people_names(df, mappings, bid_name_mappings_br)

                        if people_list is None:
                            yield error_repr(msg)
                        else:
                            yield info_repr(msg)

                            pool = ThreadPool(processes=8)
                            batch_results = {}

                            num_batches = len(df) / 10 + 1 if len(df) % 10 != 0 else len(df) / 10
                            for i in range(num_batches):
                                batch_results[i] = pool.apply_async(
                                    add_rows, (df, i * 10, min((i+1) * 10, len(df)), breeze_api, mappings))

                            error_occurred = False
                            for i in range(num_batches):
                                res = batch_results[i].get()
                                if res[0] == 0:
                                    yield error_repr(res[1])
                                    error_occurred = True
                                    break

                            if not error_occurred:
                                # add report
                                yield "<br>"
                                yield "<pre/>" + get_report(df, mappings, breeze_api)

        except RuntimeError as e:
            yield error_repr(e.message)

    templates_dir = os.path.join(bp.root_path, 'templates')
    env = Environment(loader=FileSystemLoader(templates_dir))
    tmpl = env.get_template('upload/complete.html')
    return Response(tmpl.generate(result=inner()))


def add_rows(df, start, end, breeze_api, mappings):

    for i, row in enumerate(df[start:end]):
        env_str = str(row['env_num'])
        bid, person_name = get_bid_and_name(env_str, mappings, breeze_api)

        if person_name is not None:
            json_str = json.dumps([{'name': row['fund_name'], 'amount': row['amount']}])
            contribution_params = {
                'date': row['date'],
                'name': person_name,
                'person_id': bid,
                'method': row['method'],
                'funds_json': json_str,
                'amount': row['amount'],
                'note': row['note'],
                'batch_number': row['batch_num'],
                'batch_name': row['batch_name']
            }
            pay_result = breeze_api.add_contribution(**contribution_params)
            if pay_result is None:
                message = "Failed in adding contribution! Stopped at row %d<br>" % (start+i)
                message += "Envelope #: %s\n" % (row['env_num'])
                return 0, message
        else:
            message = "Cannot find a target person! Stopped at row %d<br>" % (start + i)
            message += "Envelope #: %s\n" % env_str
            return 0, message

    return 1, "Contributions #%.3d ~ #%.3d have been processed" % (start+1, end)


def validate_dates_nums(df):
    for row in df:
        batch_num = int(row['batch_num'])
        date, month, year = row['date'].split("-")
        date, month, year = int(date), int(month), int(year)
        amount = row['amount']

        if batch_num == 0 or batch_num <= 256 or batch_num > 500:
            return None, "Check batch number ! Less than 257 or greater than 500"
        elif year > 2020 or year < 2019:
            return None, "Check year!"
        elif amount > 100000:
            return None, "Check fund amount! Greater than 100,000!"

    return df, "Finished basic validations of batch number and date"


def validate_envelop_nums(df, mappings):
    env_list = []
    for row in df:
        env_str = str(row['env_num'])
        if valid_form_envelope_id(env_str):
            if env_str not in mappings:
                msg = "Envelope # %s is not registered!<br>" % env_str
                msg += "Please register new envelope number in your excel upload form!"
                return None, msg
            else:
                env_list.append(env_str)
        elif not valid_form_breeze_id(env_str):
            return None, "Envelope ID or Breeze ID # %s is not valid!<br>" % env_str
        else:
            env_list.append(env_str)
    return env_list, "Finished validation of all envelope numbers"


def validate_people_names(df, mappings, bid_name_mappings_br):
    env_list = []
    error_msgs = ""
    for row in df:
        env_str = str(row['env_num'])
        if valid_form_envelope_id(env_str):
            bid, p_name = get_bid_and_name(env_str, mappings)

            if p_name != bid_name_mappings_br[bid]:
                msg = "[%s] Name in excel form %s is different from one in breeze systems %s !<br>" \
                      % (bid, p_name, bid_name_mappings_br[bid])
                error_msgs += msg
            else:
                env_list.append(row['env_num'])

    if error_msgs != "":
        return None, error_msgs

    return env_list, "Finished validation of names of all people"


def get_report(df, mappings, breeze_api):
    fund_summary = {}
    note_summary = defaultdict(list)
    total = 0.0

    active_funds = {}

    for row in df:
        fund_id = str(int(float(row['fund_id'])))
        fund_str = '. '.join([fund_id, row['fund_name']])
        is_special_fund = int(float(row['fund_id'])) >= 1000

        active_funds[int(fund_id)] = fund_str

        if fund_str in fund_summary:
            fund_summary[fund_str] += float(row["amount"])
        else:
            fund_summary[fund_str] = float(row["amount"])
        total += float(row["amount"])

        _, p_name = get_bid_and_name(str(row['env_num']), mappings, breeze_api)
        if p_name is not None:
            if row['note'] != "":
                note_str = "$%s\t%s" % (row['amount'], row['note'])
                note_str += " :: %s" % p_name
                note_summary[fund_str].append(note_str)
            elif is_special_fund:
                note_str = "$%s\t%s" % (row['amount'], row['fund_name'])
                note_str += " :: %s" % p_name
                note_summary[fund_str].append(note_str)

    sorted_fund_ids = sorted(active_funds.keys())
    sorted_fund_strs = []
    for fid in sorted_fund_ids:
        for fund_id in active_funds:
            if fid == fund_id:
                sorted_fund_strs.append(active_funds[fund_id])

    fund_date = datetime.strptime(df[-1]["date"], "%d-%m-%Y")
    fund_date_str = datetime.strftime(fund_date, "%Y-%m-%d")

    return build_result(df[-1]["batch_num"], fund_date_str, total, fund_summary, note_summary, sorted_fund_strs)